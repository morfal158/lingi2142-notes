.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Segment Routing
===============

This chapter will describe the operation of the recently proposed Segment Routing architecture. There are no published magazine articles that already describe Segment Routing. Most of the information on this topic is covered in Internet drafts and presentations that are available from http://www.segment-routing.net

Among these documents, the following are the best starting points :

 - C. Filsfils, `Segment Routing - Simplifying the Network`, NANOG58, see http://www.nanog.org/meetings/nanog58/agenda
 - C. Filsfils et al. , `Segment Routing Architecture`, https://datatracker.ietf.org/doc/draft-ietf-spring-segment-routing/
 - S. Previdi et al., `SPRING : Problem statement and requirements`, https://datatracker.ietf.org/doc/draft-ietf-spring-problem-statement/

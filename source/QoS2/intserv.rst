.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Integrated Services
-------------------
.. sectionauthor:: nobody

.. todo:: 

   Explain the main principles of the Integrated Services architecture and the different services that have been specified.



Reference

 - :rfc:`1633` for architectural principles (see also David D. Clark, Scott Shenker, and Lixia Zhang. 1992. Supporting real-time applications in an Integrated Services Packet Network: architecture and mechanism. In Conference proceedings on Communications architectures & protocols (SIGCOMM '92), David Oran (Ed.). ACM, New York, NY, USA, 14-26. DOI=10.1145/144179.144199 http://doi.acm.org/10.1145/144179.144199 )
 - :rfc:`2211` for Controlled Load
 - :rfc:`2212` for Guaranteed Service

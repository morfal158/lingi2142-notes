.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

The ReSource reserVation Protocol
---------------------------------
.. sectionauthor:: nobody

.. todo:: 

   Explain the principles and the operation of the RSVP protocol, focusing only on the unicast use case for Integrated services. 


References

 - :rfc:`2205` for the protocol specification (see also :rfc:`2750` for the policies)
 - Zhang, Lixia, Steve Deering, Deborah Estrin, Scott Shenker, and Daniel Zappala. "RSVP: A new resource reservation protocol." Network, IEEE 7, no. 5 (1993): 8-18.


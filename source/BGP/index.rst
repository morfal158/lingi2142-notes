.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

The Border Gateway Protocol
===========================

This chapter will describe the utilisation of iBGP as well as the BGP decision process.
.. sectionauthor:: Julie Chatelain

Core-Based Trees
================

CBT (:rfc:`2189`, :rfc:`2201`) is a multicast routing architecture that builds only one tree per group (unlike others that build one tree per sender). The tree is shared by all group's senders and receivers. Its goal is to make IP Multicast Scalable.

How does it work ?
------------------

CBT works by constructing a tree of routers with one (or multiple) core(s) : [#fcbtdef]_

- A single node (router) is the core of the tree.
- Branches emmanate from the core (root). These branches are made up of others routers.
- There is a core address and a group identifier associated with every group.
- The core address is the unicast address of the core router. This address is used to get packets to the tree.

Once on the tree, a packed is multicasted based on a globally unique group identifier (group-id).

.. figure:: imgs/cbt-example.png
   :align: center
   :scale: 80

   Example of CBT


Principles
----------

1. A single node (router) is the core (root) of the trees.
2. One single tree per group.
3. The receivers are the leafs of the tree.
4. The branches of the tree represent the shortest path between leaf and core (root).

CBT is bidirectional : It uses the `Reverse Path Forwarding`. Receivers (leafs) send and receive packets via the branch of the tree. Therefore, the same path is used to send and receive message and packets. 


Step by step
------------

- **Initialization**

 0. The core routers are statically configured.

- **Tree Formation**

    +-----------------------------------------+------------------------------------------+
    |.. figure:: imgs/cbt-tree-formation.png  |.. figure:: imgs/cbt-tree-formation2.png  |
    |   :scale: 80                            |   :scale: 80                             |
    |   :align: center                        |   :align: center                         |
    +-----------------------------------------+------------------------------------------+

..

 1. A group member appears on the network.
 2. Its local CBT router looks up the multicast address and obtains the address of the Core router for the group.
 3. The router sends a `Join Request` towards the core.
 4. The join request is forwarded to the next-hop router as determined by the unicast forwarding table.
 5. Along the way the request message creates a temporary state (entry) in all CBT routers.

  a) Typical entry contains :

   - group identifier
   - parent address (address of the router toward the core)
   - interface on which it is connected to the parent
   - number of children
   - interface associated with each child (downstream router)

  b) The router is in a state of pending membership.

 6. The join request continues its journey until it either reaches the addressed core or reaches a CBT router that is already part of the tree.

.. image:: imgs/cbt-tree-formation-new.png
   :align: center
   :scale: 80

..

 7. The core confirms the join request by sending a `Join Ack` message.
 8. The ack message travel the same way as the request message in the reverse direction.
 9. Along the way the ack message makes the temporary state permanent. The router becomes a non-core router of the tree for the group. A new branche is created.

- **Data forwarding**

.. image:: imgs/cbt-data-forwarding.png
   :align: center
   :scale: 80

..

 1. Multicast packets are send to the multicast tree using unicast routing. (That way, multicast groups and packets are invisible to routers not on the tree. These routers don't need to keep information about the groups).

  a) This is achieved by putting the group core address in the destination field and group-id int the option field of IP packet's header.

 2. Once on the tree, multicast packets span the tree based on the packet's group-id.

  a) The core address in the destination field is discarded.
  b) The group-id in the option field is placed in the destination field.

 3. CBT routers forward the packets according to the information in their CBT Forwarding Information Base.


- **Quitting**

    +-------------------------------+--------------------------------+
    |.. figure:: imgs/cbt-quit.png  |.. figure:: imgs/cbt-quit2.png  |
    |   :scale: 80                  |   :scale: 80                   |
    |   :align: center              |   :align: center               |
    +-------------------------------+--------------------------------+

..

 1. A receiver wants to quit a multicast group.
 2. The router sends a `Quit Request` message upstream.
 3. The quit request is acknowledged with a `Quit Ack` by the upstream (parent) router.
 4. If the parent router doesn't have any more child belonging to the group, it also sends a `Quit Request` upstream.

- **Path or Node Failure**

 1. The downstream routers check the liveness of their parent by periodically sending `Echo Request` message towards the parent.
 2. The parent confirms with a `Echo Reply` message.
 3. A failure is detected when the parent doesn't reply.
 4. Two options are possible:

  a) the router submits a new `Join Request` and keep the failure transparent to the downstream branch.
  b) the router tells downstream routers (by sending a `Flush-Tree` message) about the failure and allow them to independently attempt to re-attach themselvers to the tree.


Advantages and drawbacks
------------------------

**Advantages**

 1. `Scalability` : since there's only one tree per group the amount of state that needs to be stored at each router is equal to the number of groups. Routers not on the tree require no knowledge of the tree.
 2. Tree Creation is `recceiver based` : tree building is restricted only to routers interested in becoming part of the group (or on the path between a potential member and the tree). The other routers aren't involded.
 3. `Independent of the underlying unicast routing algorithm` : it results in a simplified multicast tree formation across domain boundaries.

**Drawbacks**

 1. `Core Placement` : CBT doesn't always provide the most optimal paths between members of a group.
 2. The Core as a `Single Point of Failure` : can be solved with multiple cores but it increases the complexity.

  a) Single Core CBT Trees : We have multiple backup cores. If the primary core fails, we use the same recovery scenario as in the case of path or node failure.
  b) Multiple Core CBT Trees : Subsets of tree attached to each core routers. It can also optimize routes between the members. It needs a explicit protocol amongst the backup cores to handle failure.


.. rubric:: References

.. [#fcbtdef] http://web.archive.org/web/20051201222035/http://www.cse.iitk.ac.in/research/mtech1997/9711105/node15.html
              http://en.wikipedia.org/wiki/Core-based_trees
              http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.481.8667&rep=rep1&type=pdf
              http://www.cse.iitk.ac.in/users/braman/courses/cs625-fall2003/lec-notes/lec-notes19-1.html


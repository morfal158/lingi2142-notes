.. sectionauthor:: Julie Chatelain

Protocol Independent Multicast - Sparse Mode
============================================

PIM-SM is called `Protocol Independent` (:rfc:`4601`) because it is not dependent on any particular unicast routing protocol for topology discovery. It is called `Sparse Mode` because it assumes that the recipients for the multicast group will be sparsely distributed throughout the network.


How does it work ?
------------------

It builds unidirectional shared trees rooted at a `Rendez-vous Point` (RP) per group. It optionally creates shortest-path trees per source. It is a protocol created for efficiently routing packets to multicast groups that span wide-area and inter-domain internet. [#fpimdef]_

.. figure:: imgs/pim-example.png
   :align: center
   :scale: 80

   Example of PIM working

..

**Roles of the Rendez-vous Point**

 1. It is the root of the non-source-specific shared trees (multicast distribution trees) for the receivers.
 2. It is the leaf of the source-specific tree for the senders.
 3. It receives Join messages from receivers.
 4. It receives data from the senders and forwards it onto the shared tree.
 5. This is the point where the receivers meet the sources.


Events
------

- When a new receiver wants to join the group

.. image:: imgs/pim-receiver-joining.png
   :align: center
   :scale: 80

..

 1. One of the receiver's local routers is elected as the Designated Router (DR).
 2. The DR sends a PIM `Join` message toward the RP for that multicast group.
 3. The Join message travels hop-by-hop towards the RP for the group. Each router it passes through instanciates `multicast tree state` [(\*,G) State] for the group and becomes part of the Shared Tree.
 4. It finally reaches the RP (or a router that already has a `Join state` for that group). The RP adds the interface on which it has received the message to the outgoing list of the RP Tree forwarding state entry.
 5. Join messages are resent periodically as long as the receiver stays in the group.

- When the receivers want to leave the group

  The DR sends a PIM `Prune` message toward the RP for the multicast group. Even if the `Prune` message is not correctly sent, the state will eventually time out.

- When another sender starts

.. image:: imgs/pim-receiver-registering.png
   :align: center
   :scale: 80

..

 1. The sender starts sending data destined for a multicast group.
 2. The sender's local router (DR) takes the data packets and unicast-encapsulates (registers) them.
 3. The DR sends the encapsulated packets (PIM `Register` packets) to the RP (via unicast).
 4. The RP receives the PIM `Register` packets and decapsulates them.

  a) If the data rate is high, the RP sends a PIM `Join` message toward the sender to build a Source Tree. Each router along the way instanciates a (S,G) State.
  b) The packets from the sender will start to flow along the Source Tree.
  c) When the RP starts receiving packets via the Source Tree, it sends a `Register-Stop` message back to the sender's DR to prevent the DR from unnecessarily encapsulating the packets.

    +-----------------------------------------+------------------------------------------+
    |.. figure:: imgs/pim-sender-joining.png  |.. figure:: imgs/pim-sender-stopping.png  |
    |   :scale: 80                            |   :scale: 80                             |
    |   :align: center                        |   :align: center                         |
    +-----------------------------------------+------------------------------------------+

 5. The RP forwards the packets onto the shared tree.
 6. The packets follow the multicast tree state in the routers on the RP Tree. The packets are replicated when the branch forks.
 7. The packets reach all the receivers for the multicast group.

.. Note:: if the RP has no receivers for the data send by a source, it still adds the source into the PIM table but then sends a `Register-Stop` message.


Optimality
----------

- Is an RP rooted tree optimal ?

    No. The path from the sender to the RP to the receivers may not be the shortest one.
    To solve this problem, it is possible for the receivers to get content directly from the sender. To do this, it issues a (S,G) `Join` towards the source. It will instanciates a (S,G) State on the routers along the path. When the `Join` message reaches the source (or a router that already has a (S,G) State), the shortest-path tree (SPT) is finished. 


.. image:: imgs/pim-receiver-joining-spt.png
   :align: center
   :scale: 80

..

    The data packets from the source start flowing along the SPT. When they reach the receiver, the DR will drop packets from the RPT and sends a (S,G) Prune message towards the RP. 


.. image:: imgs/pim-receiver-pruning.png
   :align: center
   :scale: 80

..

    The `Prune` message travels hop-by-hop toward the RP and indicates to the routers along the way that traffic from S to G should not be forwarded in this direction. It continues until it reaches the RP or a router that still needs the traffic from the source for another receiver.

.. image:: imgs/pim-receiver-spt.png
   :align: center
   :scale: 80

..


RP Address Distribution
-----------------------

It can be done with a bootstrap mechanism or by static configuration.
With the Bootstrap Router (BSR) mechanism :

 1. One router in each PIM domain is elected the BSR.
 2. All the routers that are RP candidates periodically unicast their candidacy to the BSR.
 3. The BSR picks a RP-set and periodically announces this set in a message.

To map a group to a RP, a router uses a hash function to hash the group address into the RP-set.


Advantages
----------

 1. Scalability : PIM-SM scales rather well for wide-area usage. Since it uses a shared tree, it uses less memory (only equivalent to the number of group).
 2. Unicast routing protocol-independent.
 3. Since PIM-SM can optimise its trees after formation (by creating a shortest-path tree between sender and receiver), the RP location is less critical than the Core location in CBT.


.. rubric:: References

.. [#fpimdef] http://en.wikipedia.org/wiki/Protocol_Independent_Multicast


http://www.juniper.net/documentation/en_US/junos13.3/topics/concept/multicast-pim-sparse-characteristics.html
http://www.cl.cam.ac.uk/~jac22/books/mm/book/node79.html
